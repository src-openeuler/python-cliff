%global _empty_manifest_terminate_build 0
Name:		python-cliff
Version:	4.8.0
Release:	1
Summary:	Command Line Interface Formulation Framework
License:	Apache-2.0
URL:		https://docs.openstack.org/cliff/latest/
Source0:	https://files.pythonhosted.org/packages/62/44/e9073c882c9765074a1043f5004f31fd97c5c6571836e59987fa28781805/cliff-4.8.0.tar.gz
BuildArch:	noarch

%description
cliff is a framework for building command line programs. It uses
entry points to provide subcommands, output formatters, and other extensions.

%package -n python3-cliff
Summary:	Command Line Interface Formulation Framework
Provides:	python-cliff = %{version}-%{release}
# Base build requires
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
# General requires
BuildRequires:	python3-prettytable
BuildRequires:	python3-pyyaml
BuildRequires:	python3-autopage
BuildRequires:	python3-cmd2
BuildRequires:	python3-pyparsing
BuildRequires:	python3-stevedore
# General requires
Requires:	python3-prettytable
Requires:	python3-pyyaml
Requires:	python3-autopage
Requires:	python3-cmd2
Requires:	python3-importlib-metadata
Requires:	python3-stevedore
%description -n python3-cliff
cliff is a framework for building command line programs. It uses
entry points to provide subcommands, output formatters, and other extensions.

%package help
Summary:	Development documents and examples for cliff
Provides:	python3-cliff-doc
%description help
cliff is a framework for building command line programs. It uses
entry points to provide subcommands, output formatters, and other extensions.

%prep
%autosetup -n cliff-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-cliff -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Nov 29 2024 liuzhilin <liuzhilin@kylinos.cn> - 4.8.0-1
- Remove unnecessary shebangs
- ruff: Enable pyupgrade rules
- Migrate to ruff, ruff-format
- Drop support for Python 3.8, add Python 3.12
- pre-commit: Bump versions
- Normalize columns given by '-c'/'--columns'

* Mon Jun 24 2024 liyue01 <liyue01@kylinos.cn> - 4.7.0-1
- Update package to version 4.7.0
- Add fixtures explicitly in test requirements

* Mon Mar 25 2024 wangqiang <wangqiang1@kylinos.cn> - 4.6.0-1
- Update package to version 4.6.0

* Wed Jul 19 2023 Dongxing Wang <dxwangk@isoftstone.com> - 4.3.0-1
- Update package to version 4.3.0

* Fri Mar 10 2023 wangjunqi <wangjunqi@kylinos.cn> - 4.2.0-1
- Update package to version 4.2.0

* Wed Nov 23 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 4.1.0-1
- Update package to version 4.1.0

* Tue Jul 12 2022 OpenStack_SIG <openstack@openeuler.org> - 3.10.1-1
- Upgrade package python3-cliff to version 3.10.1

* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 3.7.0-1
- Upgrade to version 3.7.0
* Sat Jan 30 2021 zhangy <zhangy1317@foxmail.com>
- Add buildrequires
* Fri Nov 20 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
